import React, { Component } from "react";
import axios from "axios";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardTitle, CardText, CardSubtitle, Form, FormGroup, Label, Input} from 'reactstrap';

const MySwal = withReactContent(Swal);

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      username: "",
      password: ""
    };

    this.initStream = this.initStream.bind(this);
    this.validarForm = this.validarForm.bind(this);
  }
  async validarForm() {
    if(this.state.username.length<4 || this.state.password < 6){
      MySwal.fire({
          title: <p>Error</p>,
          icon: 'error',
          text: "Por favor verifica los datos del formulario!"
      });
      console.log("formularo invalido.");
    }else {
        this.initStream(this);
    }

}
  async initStream() {
    await this.setState({
      loading: true
    });

    const base = "http://34.192.0.78";

    const formData = new FormData();
    formData.set("username", this.state.username);
    formData.set("password", this.state.password);

    await axios({
      method: "POST",
      url: `${base}/auth/token/login/`,
      data: formData,
      config: {
        headers: { "Content-Type": "multipart/form-data" }
      }
    }).then(authorization => {
        console.log(authorization.data);
        localStorage.setItem("token", authorization.data.stream_token);
        localStorage.setItem("user_id", authorization.data.user_id);
        localStorage.setItem("user_name", authorization.data.user_name);
        this.setState({
        loading: false
        });

        this.props.history.push("/");
    }).catch(error => {
        MySwal.fire({
            title: <p>Error</p>,
            icon: 'error',
            text: "No se puede iniciar sesión con las credenciales proporcionadas!"
        });
    });

    
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div className="login-root">
        <Card>
            <CardBody>
                <CardTitle tag="h5">Iniciar Sesión</CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">Página de inicio de sesión del Chat Grupal</CardSubtitle>
            </CardBody>
            <CardBody>
                <Form>
                    <FormGroup>
                        <Label for="username">Nombre de usuario</Label>
                        <Input 
                            type="text" 
                            name="username" 
                            id="username" 
                            placeholder="Nombre de usuario " 
                            onChange={e => this.handleChange(e)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Contraseña</Label>
                        <Input 
                            type="password" 
                            name="password" 
                            id="password" 
                            placeholder="Contraseña del usuario" 
                            onChange={e => this.handleChange(e)}
                        />
                    </FormGroup>
                    
                    
                    <Button onClick={this.validarForm}>Iniciar</Button>
                    <CardText>Si aun no te has registrado, puedes hacerlo siguiendo el enlace de abajo.</CardText>
                    <Link to="/registrarse"> Registrarse</Link>
                </Form>
            </CardBody>
        </Card>
      </div>
    );
  }
}

export default Login;