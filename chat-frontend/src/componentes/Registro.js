import React, { Component } from "react";
import axios from "axios";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { Link } from "react-router-dom";
import { Button, Card, CardBody, CardTitle, CardSubtitle, CardText, Form, FormGroup, Label, Input} from 'reactstrap';
const MySwal = withReactContent(Swal);

class Registro extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      username: "",
      password: ""
    };
    this.validarForm = this.validarForm.bind(this);
    this.initStream = this.initStream.bind(this);
  }

  async validarForm() {
      if(this.state.username.length<4 || this.state.password < 6){
        MySwal.fire({
            title: <p>Error</p>,
            icon: 'error',
            text: "Por favor verifica los datos del formulario!"
        });
        console.log("formularo invalido.");
      }else {
          this.initStream(this);
      }
  }
  async initStream() {
    await this.setState({
      loading: true
    });

    //si se corre en localhost definir base = "http://localhost:8000";
    const base = "http://34.192.0.78"; //url del back-end
    //creamos un objeto FormData
    const formData = new FormData();
    //agregamos los datos del formulario a nuestro FormData
    formData.set("username", this.state.username);
    formData.set("password", this.state.password);
    //hacemos una petición de tipo POST al rest de django, para crear un usuario
    //con los datos utilizados en el formulario
    await axios({
        method: "POST",
        url: `${base}/auth/users/`,
        data: formData,
        config: {
          headers: { "Content-Type": "multipart/form-data" }
        }
    }).then(response=> {
        //imprimimos en consola la respuesta de la petición
        console.log(response);
        MySwal.fire({
          title: <p>Cuenta creada</p>,
          icon: 'success',
          text: "Tu ha creado tu cuenta de manera exitosa!"
        }).then(()=>{
          this.setState({
            loading: false
          });
          //redirigimos a la vista de login, para que el usuario puede iniciar sesión.
          this.props.history.push("/login");
        });
        
    }).catch(error=> {
        MySwal.fire({
            title: <p>Error</p>,
            icon: 'error',
            text: "No es posible crear un usuario con una contraseña tan poco segura!"
        });
    });
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div className="login-root">
        <Card>
            <CardBody>
                <CardTitle tag="h5">Registrarse</CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">Página de registro del Chat Grupal</CardSubtitle>
            </CardBody>
            <CardBody>
                <Form>
                    <FormGroup>
                        <Label for="username">Nombre de usuario</Label>
                        <Input 
                            type="text" 
                            name="username" 
                            id="username" 
                            placeholder="Nombre de usuario " 
                            onChange={e => this.handleChange(e)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Contraseña</Label>
                        <Input 
                            type="password" 
                            name="password" 
                            id="password" 
                            placeholder="Contraseña del usuario" 
                            onChange={e => this.handleChange(e)}
                        />
                    </FormGroup>
                    
                    
                    <Button onClick={this.validarForm}>Registro</Button>
                    <CardText>Si aun no te has registrado, puedes hacerlo siguiendo el enlace de abajo.</CardText>
                    <Link to="/login"> Iniciar Sesión</Link>
                </Form>
            </CardBody>
        </Card>
      </div>
    );
  }
}

export default Registro;