import React, { Component } from "react";
import {
    Chat,
    Channel,
    ChannelHeader,
    Thread,
    Window, 
    Streami18n
} from "stream-chat-react";
import {
    esTranslations,
  } from 'stream-chat-react';
import { MessageList, MessageInput } from "stream-chat-react";
import { StreamChat} from "stream-chat";
import { Button, ButtonGroup } from 'reactstrap';

import "stream-chat-react/dist/css/index.css";
const i18n = new Streami18n();
i18n.registerTranslation('es', esTranslations);
i18n.setLanguage('es');
//podemos traducir también siertas textos eplicitamente
/* const i18n = new Streami18n({
    language: 'co',
    translationsForLanguage: {
      'Delete': 'Eliminar',
      'Attach files' : 'Adjuntar imagen',
      'Type your message' : 'Escribe tu mensaje',
      'Nothing yet...': 'Nada aun ...',
      '{{ firstUser }} and {{ secondUser }} are typing...':
        '{{ firstUser }} y {{ secondUser }} estan escribiendo...',
    },
  }); */
function formatFecha(date){
    var meses = [
        "Ene", "Feb", "Mar",
        "Abr", "May", "Jun", "Jul",
        "Ago", "Sep", "Oct",
        "Nov", "Dic"
      ]
    var dia = date.getDate();
    var mes = date.getMonth();
    var hr = date.getHours() < 10 ? '0' + date.getHours(): date.getHours();
    var min = date.getMinutes();
    return dia + ' ' + meses[mes] + ' - ' + hr + ':' + min ;
}
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usuarios: []
          };
        this.client = new StreamChat('h7htte9n9cjn');
        this.obtenerUsuarios();

        const userToken = localStorage.getItem("token");
        const tokenUserId = localStorage.getItem("user_id");
        const tokenUserName = localStorage.getItem("user_name");
        
        this.client.connectUser(
            {
                id: tokenUserId,
                name: tokenUserName,
                image: 'https://getstream.io/random_svg/?id=+' + tokenUserId + '+&name=' + tokenUserName
            },
            userToken
        );
        
        this.channel = this.client.channel("publico2", "ChatTest3", {
            image:
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGpEJq8PnT_SqDgcyvxpJXt75jpKWhg7N-1g&usqp=CAU",
            name: "Mini Chat Grupal",
            session: 8
        });

        this.obtenerUsuarios = this.obtenerUsuarios.bind(this);
        this.cerrarSesion = this.cerrarSesion.bind(this);
    }
    async obtenerUsuarios() {
        const res = await this.client.queryUsers({ role : "user"});
        console.log(res.users);
        res.users.forEach(user=>{
            var date = new Date(user.last_active);
            user.last_active = formatFecha(date);
        });
        await this.setState({
            usuarios : res.users
        });
        console.log("estatus:")
        console.log(this.state.usuarios);
    }
    async cerrarSesion() {
        await this.client.disconnectUser(); 
        
        localStorage.removeItem("token");
        localStorage.removeItem("user_id");
        localStorage.removeItem("user_name");
        this.props.history.push("/login");
    }
    
    render() {
        if(this.state.usuarios){
            var usuarios = this.state.usuarios.map((usuario, i) => {
                return <li key={usuario.id} className="list-group-item list-group-item-success">
                        <img 
                            alt ={usuario.name}
                            className="rounded float-left redondo" 
                            src={usuario.image}
                            width="50"
                        />
                        <b className="mr-3 ml-3">{usuario.name}</b> 
                        <span className=" ml-3 badge badge-success badge-pill">{usuario.last_active}</span>
                    </li>
            });
        }
        return (
            <div className="container-fluid row">
                <div className="col-sm-12 col-md-6 col-lg-6">

                    <ul className="list-group">
                        <li class="list-group-item active" aria-current="true">Lista de usuarios registrados</li>
                        {usuarios}
                    </ul><br/>
                    <ButtonGroup>
                        <Button color="info" onClick={this.obtenerUsuarios}>Actualizar lista</Button>
                        <Button color="danger" onClick={this.cerrarSesion}>Cerrar Cesión</Button>
                    </ButtonGroup>                   

                </div>
                <div className="col-sm-12 col-md-6 col-lg-6">
                    <Chat 
                        client={this.client} 
                        i18nInstance={i18n}
                        theme={"team light"}
                        
                    >
                        <Channel 
                            channel={this.channel}
                            acceptedFiles={['image/*']}
                            maxNumberOfFiles={1}
                            multipleUploads={false}
                            EmptyPlaceholder
                        >
                            <Window>
                                <ChannelHeader />
                                <MessageList 
                                    unsafeHTML
                                    messageActions={['delete']}
                                />
                                <MessageInput />
                            </Window>
                            <Thread />
                        </Channel>
                    </Chat>
                </div>
            </div>
            
        );
    }
}

export default App;