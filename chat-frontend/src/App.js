import React from "react";
//importamos los componentes necesarios de react-router-doom para establecer las rutas de nuestro frontend
import { BrowserRouter as Router, Switch } from "react-router-dom";
//importamos los componentes necesarios para que nuestra aplicación de react funcione correctamente
import Chat from "./componentes/Chat";
import Login from "./componentes/Login";
import Registro from "./componentes/Registro";
//estos componentes son necesarios para validar si el usuario actual ya inicio sesión o no
import UnauthedRoute from "./componentes/UnauthedRoute";
import AuthedRoute from "./componentes/AuthedRoute";
//Inicializamos la app
const App = () => (
  <Router>
    <Switch>
      <UnauthedRoute path="/registrarse" component={Registro} />
      <UnauthedRoute path="/login" component={Login} />
      <AuthedRoute path="/" component={Chat} />
    </Switch>
  </Router>
);

export default App;