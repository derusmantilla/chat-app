from django.contrib import admin
from django.urls import path, include, re_path
from . import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('', views.inicio, name='inicio'),
    re_path(r'^(?:.*)/?$', views.inicio, name='inicio'),
]